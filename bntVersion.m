function ver = bntVersion()
    ver = git('describe --long --all --always');
    % if length(ver) == 7, then there was no tag, we have just a hash
    hyphenInd = strfind(ver, '-'); % should be two
    verId = ver(1:hyphenInd(1)-1);
    passtCommits = ver(hyphenInd(1)+1:hyphenInd(2)-1);
    sha = ver(hyphenInd(2)+1:end);
end